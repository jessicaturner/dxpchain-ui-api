/*
* Markets listed in the Dashboard page. False indicates the price needs to be
* flipped to be displayed properly
*/
 const marketsByChain = {
    "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2": { // DXP chain id first 10
        markets: [
            ["DXP", "CNY"],
            ["OPEN.BTC", "DXP", false],
            ["OPEN.BTC", "OPEN.STEEM"],
            ["DXP", "ICOO"],
            ["DXP", "BLOCKPAY"],
            ["DXP", "OBITS"],
            ["DXP", "USD"],
            ["DXP", "GOLD"],
            ["DXP", "SILVER"],
            ["DXP", "BKT"],
            ["OPEN.BTC", "OPEN.DGD", false],
            ["DXP", "BTWTY"],
            ["DXP", "DXPR"],
            ["OPEN.BTC", "OPEN.INCNT", false],
            [ "DXP", "OPEN.ETH"],
            ["CNY", "USD"]
        ],
        newAssets: [
            "CENTZ"
        ]
    },
    "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2": { // TESTNET chain id first 10
        markets: [
            ["TEST", "PEG.FAKEUSD"],
            ["TEST", "BTWTY"],
            ["TEST", "PEG.PARITY"]
        ],
        newAssets: [
        ]
    }
}

export { marketsByChain };
